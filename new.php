<?php
	include 'lib/Mobile_Detect.php';
	$detect = new Mobile_Detect();
?>
<!doctype html>
<html lang="az">
<head>

    <meta charset="UTF-8">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-THXMJ3Z');</script>
    <!-- End Google Tag Manager -->

    
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THXMJ3Z"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/img/favicon.png?v=1.0" />
    <title>CİB</title>

    <meta property="og:url"                content="https://cib.az/new" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Cib Yeniləndi" />
    <meta property="og:description"        content="Bütün ödəniş və pul köçürmələrinizi asan, rahat və təhlükəsiz CİB mobil tətbiqi ilə edin." />
    <meta property="og:image"              content="https://cib.az/assets/img/new/banner_desktop.jpg" />

    

    <link rel="stylesheet" href="assets/css/bxslider/4.2.12/jquery.bxslider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="assets/css/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome-5.11.2/css/all.min.css">
    <link rel="stylesheet" href="assets/css/main.css">


    <style>
        .bx-wrapper{
            border: 0; padding: 0; margin: 0; box-shadow: none;
        }
    </style>
       <!-- Google Tag Manager -->
       <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
             'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
               })(window,document,'script','dataLayer','GTM-MNN9BTV');</script>
<!-- End Google Tag Manager -->
</head>
<body>
 <!-- Google Tag Manager (noscript) -->
 <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNN9BTV"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="preloader"><img src="assets/img/logo.svg" alt=""></div>

    <div style="min-height: 100%" class="container-fluid p-0 agreement">

        <header>
            <img class="inner-page-banner" src="assets/img/bg/slider-yellow-bg.png" alt="">
            <img class="d-none mobile-inner-page-banner" src="assets/img/bg/page-mobile-banner.png" alt="">
            <div class="container">
                <div class="row py-3 my-md-4">
                    <div class="text-center text-md-left col-12 col-md-4">
                        <a class="text-center text-md-left logo" href="https://findynamix.com/">
                            <img src="assets/img/logo.svg" alt="">
                            <img src="assets/img/logo.svg" alt="">
                        </a>
                        <span class="slogan">
                            ASAN RAHAT TƏHLÜKƏSİZ
                        </span>
                    </div>
                    <div class="d-none d-md-block col-md-8">
                        <ul class="social-icons">
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://facebook.com/cib.azerbaijan/"><i class="fab fa-facebook-square"></i></a>
                            </li>
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://www.instagram.com/cib.az/"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://www.youtube.com/channel/UCXLN86R1L1W6N4d-mdp1ATQ"><i class="fab fa-youtube-square"></i></a>
                            </li>
                            <li>
                                    <a class="ml-3 ml-md-4" target="_blank"
                                        href="https://www.linkedin.com/company/findynamix/"><i
                                            class="fab fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a class="ml-3 ml-md-4" target="_blank"
                                        href="https://twitter.com/cibim_az"><i
                                            class="fab fa-twitter"></i></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        <section class="page-wrapper agreement">

            
            <div class="container-fluid">
                <div class="row">

                    <div class="col-12 page-title cap">
                        <div class="container">
                            <div class="caption">
                                <h1><span class="d-none d-md-inline-block">CİB</span><b class="color-1">Yeniləndi</b></h1>
                            </div>
                        </div>
                    </div>

                    <div class="d-none d-md-block p-0 col-12 header-banner text-center">
                        <img width="100%" src="assets/img/new/banner_desktop.jpg" alt="">
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <!--<p class="d-block d-md-none m-3">Müştəri rahatlığının əsas götürüldüyü yenilənmiş CİB artıq daha sadə, sürətli və rahatdır. Cibində olsun, lazım olar.</p>-->
                        <div class="video-slider-container m-auto mr-md-3 float-none float-md-right">
                            <!--<img style="display:none; position:absolute; z-index: 1; width: calc(100% + 70px); top: -50px; left: -35px" src="assets/img/new/phone_desk_2.png" alt="">-->
                            <ul class="bxslider">
                                <!--<li class="first" data-desc="Xidmətdə fasilə yaranmamağı üçün CİBə öncədən qeydiyyatdan keç. Yeni CİB aktiv olan kimi telefonuna yüklənsin.">
                                    <video style="width: 100%" muted preload="" loop="" autoplay class="video-bg">
                                        <source src="assets/video/3x4/1_qeydiyyat.webm" type="video/webm">
                                    </video>
                                </li>-->

                                <li class="first" data-desc="Multibank funksiyası ilə bütün kartlar bir CİBdə toplanır. ">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/2_cibə_bank_kartının_əlavə_olunması.webm" type="video/webm">
                                        <source src="assets/video/3x4/2_cibə_bank_kartının_əlavə_olunması.mp4" type="video/mp4">
                                    </video>
                                </li>

                                <li data-desc="CİB ilə ödənişlər asandır.">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/3_cib_ilə_ödənişlər_çox_asandır.webm" type="video/webm">
                                        <source src="assets/video/3x4/3_cib_ilə_ödənişlər_çox_asandır.mp4" type="video/mp4">
                                    </video>
                                </li>

                                <li data-desc="Yeni CİBdə istifadəçilərin rahatlığı əsas götürüldü. Pul köçürmələri artıq daha sürətli və rahatdır.">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/4_cib_ilə_pul_köçürmələri_daha_sürətlidir.webm" type="video/webm">
                                        <source src="assets/video/3x4/4_cib_ilə_pul_köçürmələri_daha_sürətlidir.mp4" type="video/mp4">
                                    </video>
                                </li>

                                <li data-desc="CİBlə xərclərini izlə, büdcənə nəzarət et. ">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/5_cibdəki_şəxsi_məlumatlarınızı_doldurmağı_unutmayın.webm" type="video/webm">
                                        <source src="assets/video/3x4/5_cibdəki_şəxsi_məlumatlarınızı_doldurmağı_unutmayın.mp4" type="video/mp4">
                                    </video>
                                </li>

                                <li data-desc="CİBdəki şəxsi məlumatlarını doldurmağı unutma. CİB hesabının təhlükəsizliyini tam qorumaq üçün adi parolla yanaşı FingerPrint funksiyasından istifadə edərək barmaq izi yoxlamanı da seçə bilərsən.">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/6_cibdəki_ödənişlərin_tarixçəsi.webm" type="video/webm">
                                        <source src="assets/video/3x4/6_cibdəki_ödənişlərin_tarixçəsi.mp4" type="video/mp4">
                                    </video>
                                </li>

                                <li data-desc="Davamlı ödənişlərini şablonlar vasitəsilə daha rahat et.">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/7_cib_şablonları_ilə_ödənişləri_etmək_çox_sadədir.webm" type="video/webm">
                                        <source src="assets/video/3x4/7_cib_şablonları_ilə_ödənişləri_etmək_çox_sadədir.mp4" type="video/mp4">
                                    </video>
                                </li>

                                <li data-desc="Mobil, internet, telefon, TV, kommunal, dövlət və çox sayda digər ödənişləri elə, kartdan karta pul köçür, balansa və çıxarışlara bax.">
                                    <video style="width: 100%" muted preload="" loop="" class="video-bg" playsinline>
                                        <source src="assets/video/3x4/8_yeni_cib_maliyyə_köməkçinizi_qarşılayın.webm" type="video/webm">
                                        <source src="assets/video/3x4/8_yeni_cib_maliyyə_köməkçinizi_qarşılayın.mp4" type="video/mp4">
                                    </video>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="mt-5 col-12 col-md-6">
                        <p class="text-center text-md-left" id="slide-desc"></p>
                        <div id="bx-pager">
                            <a href="" data-slide-index="0" class="bx-pager-link active"></a>
                            <a href="" data-slide-index="1" class="bx-pager-link"></a>
                            <a href="" data-slide-index="2" class="bx-pager-link"></a>
                            <a href="" data-slide-index="3" class="bx-pager-link"></a>
                            <a href="" data-slide-index="4" class="bx-pager-link"></a>
                            <a href="" data-slide-index="5" class="bx-pager-link"></a>
                            <a href="" data-slide-index="6" class="bx-pager-link"></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container">
                <div class="row button-box mt-md-5">
                    <div class="col-12 text-center">
                        <h4 class="download-now">İndi yüklə</h4>
                        <?php if ($detect->isMobile() AND $detect->is('iphone')) :?>
                            <a class="" target="_blank" href="https://apps.apple.com/us/app/cib-az/id1541577214"><img src="assets/img/btn/btn-app-store-d.png" alt=""></a>
                        <?php elseif($detect->isMobile() AND !$detect->is('iphone')) : ?>
                            <!-- <a class="" target="_blank" href="https://play.google.com/store/apps/details?id=az.cib.app"><img src="assets/img/btn/btn-google-play-d.png" alt=""></a> -->
                        <?php else : ?>
                        <!-- <a class="" target="_blank" href="https://apps.apple.com/us/app/cib-az/id1541577214"><img src="assets/img/btn/btn-app-store-d.png" alt=""></a> -->
                        <!-- <a class="" target="_blank" href="https://play.google.com/store/apps/details?id=az.cib.app"><img src="assets/img/btn/btn-google-play-d.png" alt=""></a> -->
                        <?php endif; ?>
                        <a target="_blank" href="https://apps.apple.com/us/app/cib-az/id1541577214" class="mr-2">
                                <img class="download-btn" src="assets/img/btn/m-btn-app-store.png" alt="">
                            </a>
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=az.cib.app" class="ml-2">
                                <img class="download-btn" src="assets/img/btn/m-btn-google-play.png" alt="">
                            </a>
                            <a target="_blank" href="https://appgallery.huawei.com/#/app/C104070083" class="ml-2">
                                <img class="download-btn" src="assets/img/btn/mobile-huawei1.png" alt="">
                            </a>
                    
                    </div>
                </div>
            </div>

        </section>

        <footer class="footer-two">
            <div class="container">
                <div class="row pb-3 pb-md-3">
                    <div class="left col-6 col-md-4">
                        <span class="d-none d-md-block">
                            <a href="agreement.html" class="m-0">İSTİFADƏÇİ RAZILAŞMASI</a>
                        </span>
                        <span>
                            <a href="tel:*8282">
                                <img class="call-icon pl-3 pl-md-4" width='100px' src="assets/img/callcenter.png" alt="">
                            </a>
                        </span>
                    </div>
                    <div class="col-6 col-md-4 copyright text-center">
                        <a target="_blank" href="https://findynamix.com/"><p>FinDynamix © 2020</p></a>
                    </div>
                    <div class="right col-6 col-md-4 pb-2 pb-md-2">
                        <form onsubmit="return false" class="d-none d-md-block">
                            <span data-toggle="modal" data-target="#exampleModalCenter">
                                <input type="text" placeholder="e-mail">
                                <button>ABUNƏ OLUN</button>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
            <img class="d-none d-md-block" style="width: 100%; position: absolute; bottom: 0; z-index: -1;" src="assets/img/bg/footer-bg.svg" alt="">
            <img class="d-block d-md-none" style="width: 100%" src="assets/img/bg/m-footer.png" alt="">
            
        </footer>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="assets/css/bootstrap-4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <script src="assets/js/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/jquery.fitvids.js"></script>


    <script>
        $(document).ready(function(){
            $('.bxslider').bxSlider({
                video: true,
                useCSS: false,
                responsive: true,
                controls: false,
                pagerCustom: '#bx-pager',
                onSliderLoad:function(currentIndex){
                    $("video").trigger("play");
                },
                onSlideBefore:function (newIndex){
                    $('#slide-desc').html(newIndex.data('desc')).addClass(['animate__animated', 'animate__fadeIn']);
                },

                onSlideAfter:function (newIndex){
                    $('#slide-desc').removeClass(['animate__animated', 'animate__fadeIn']);
                }
            });

            $('#slide-desc').html($('.bxslider li.first').data('desc'));


            $('.preloader').hide();
        });
    </script>
</body>
</html>