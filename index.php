<?php
	include 'lib/Mobile_Detect.php';
	$detect = new Mobile_Detect();
	
	/*if ($detect->isMobile()) {
		//header('Location: new.php');
		//exit(0);
	}*/
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/img/favicon.png?v=1.0" />
    <title>FinDynamix</title>

    <meta property="og:url"                content="https://findynamix.com/" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Cib Yeniləndi" />
    <meta property="og:description"        content="Bütün ödəniş və pul köçürmələrinizi asan, rahat və təhlükəsiz CİB mobil tətbiqi ilə edin." />
    <meta property="og:image"              content="https://cib.az/assets/img/new/banner_desktop.jpg" />

    
    
    <link rel="stylesheet" href="assets/css/bxslider/4.2.12/jquery.bxslider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="assets/css/bootstrap-4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/fontawesome-5.11.2/css/all.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- Google Tag Manager -->
       <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
             'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
               })(window,document,'script','dataLayer','GTM-MNN9BTV');</script>
<!-- End Google Tag Manager -->
</head>
<body>
 <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MNN9BTV"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="preloader"><img src="assets/img/logo.png" alt=""></div>

    <div style="min-height: 100%" class="container-fluid p-0">

        <!--<img style="width:60%; position: absolute; top: 0; right: 0" src="assets/img/bg/header-bg.svg" alt="">-->

        <header>
            <div class="container">
                <div class="row py-3 my-md-4">
                    <div class="col-4">
                        <a class="logo" href="https://findynamix.com/">
                            <img src="assets/img/logo.png" alt="">
                            <img src="assets/img/logo-mobile.svg" alt="">
                        </a>
                    </div>
                    <div class="col-8">
                        <ul class="social-icons">
                        <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://www.linkedin.com/company/findynamix/"><i class="fab fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://twitter.com/cibim_az"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://facebook.com/cib.azerbaijan/"><i class="fab fa-facebook-square"></i></a>
                            </li>
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://www.instagram.com/cib.az/"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li>
                                <a class="ml-3 ml-md-4" target="_blank" href="https://www.youtube.com/channel/UCXLN86R1L1W6N4d-mdp1ATQ"><i class="fab fa-youtube-square"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>


        <div class="slider-container">
            <div class="slider">

                <div class="slider-item slide-item-1">
                    <div class="row">
                        <div class="col-sm-12 offset-md-4 col-md-8 p-md-0">
                            <img class="slider-overlay-1" src="assets/img/bg/slider-yellow-bg.png" alt="">
                            <img class="slider-overlay-2" src="assets/img/bg/slider-yellow-bg-bottom.png" alt="">

                            <img class="m-slider-overlay-1" src="assets/img/bg/slider-mobile-yellow-bg.png" alt="">
                            <img class="m-slider-overlay-2" src="assets/img/bg/slider-mobile-yellow-bg-bottom-2.png" alt="">
                        </div>

                        <div class="d-block d-md-none col-md-12">
                            <p class="m-title-3" style="color: #fff">Multi-bank maliyyə köməkçiniz</p>
                        </div>

                        <div class="d-block d-md-none col-md-12">
                            <img class="m-phones" src="assets/img/slider/slider-1/slider-1-phones.png" alt="">
                        </div>

                        <div class="d-block d-md-none col-md-12 m-btn-box text-center" style="margin-top:50%;margin-bottom:50%;">
                            <a target="_blank" href="https://apps.apple.com/us/app/cib-az/id1541577214" class="mr-2">
                                <img class="download-btn" src="assets/img/btn/m-btn-app-store.png" alt="">
                            </a>
                            <a target="_blank" href="https://play.google.com/store/apps/details?id=az.cib.app" class="ml-2">
                                <img class="download-btn" src="assets/img/btn/m-btn-google-play.png" alt="">
                            </a>
                            <a target="_blank" href="https://appgallery.huawei.com/#/app/C104070083" class="ml-2">
                                <img class="download-btn" src="assets/img/btn/mobile-huawei1.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="d-none d-md-block container overlay-2">
                        <div class="row">
                                <div class="col-md-6">
                                    <div style="max-width: 250px; float: right; position:absolute; top: 40px; right: 50px; z-index: 9999;" class="video-container">
                                        <video style="width: 100%; border-radius: 40px;" autoplay muted>
                                            <source src="assets/video/CIB Screen test.mp4" type="video/mp4">
                                            Your browser does not support HTML video.
                                        </video>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="row text-box">

                                        <div class="col-md-5">
                                            <p class="animate__animated title-1">CİB</p>
                                        </div>
                                        <div class="col-md-7">
                                            <p class="animate__animated title-2">MOBİL TƏTBİQİ</p>
                                        </div>

                                        <div class="col-md-12">
                                            <p class="title-3" style="color: #fff">Multi-bank maliyyə köməkçiniz</p>
                                        </div>
                                        <div class="col-md-12 btn-box">
                                            <a target="_blank" href="https://apps.apple.com/us/app/cib-az/id1541577214" class="mr-4" style="display: inline-block">
                                                <img class="animate__animated download-btn" src="assets/img/slider/slider-1/btn-app-store.png" alt="">
                                            </a>

                                            <a target="_blank" href="https://play.google.com/store/apps/details?id=az.cib.app" style="display: inline-block">
                                                <img class="animate__animated download-btn" src="assets/img/slider/slider-1/btn-google-play.png" alt="">
                                            </a>
                                            <a target="_blank" href="https://appgallery.huawei.com/#/app/C104070083" class="download-btn" style="position: relative; display: inline-block">
                                            <img class="animate__animated download-btn" src="assets/img/btn/appGallery.png" alt="" style="width:160px; height:56px; margin-left:25px">
                                                 </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>



        <div class="d-block d-md-none">
            <div class="col-12 text-center mb-5">
                <form action="" onsubmit="return false" class="m-subscribe-form">
                    <input type="text" placeholder="e-mail" data-toggle="modal" data-target="#exampleModalCenter">
                    <br>
                    <button data-toggle="modal" data-target="#exampleModalCenter">ABUNƏ OLUN</button>
                </form>
            </div>
            <div class="col-12 text-center mt-5">
                <a href="" class="mr-2">
                    <img src="assets/img/btn/fb-icon.png" alt="">
                </a>
                <a href="" class="ml-2">
                    <img src="assets/img/btn/insta-icon.png" alt="">
                </a>
            </div>
            <div class="col-12 text-center agreement mt-4">
                <a href="agreement.html">İSTİFADƏÇİ RAZILAŞMASI</a>
            </div>
        </div>
        
        <footer>
            <div class="container">
                <div class="row pb-3 pb-md-3">
                    <div class="left col-6 col-md-4">
                        <span class="d-none d-md-block">
                            <a href="agreement.html" class="m-0">İSTİFADƏÇİ RAZILAŞMASI</a>
                        </span>
                        <span>
                            <a href="tel:*8282">
                                <img class="call-icon pl-3 pl-md-4" src="assets/img/call-center.png" alt="">
                            </a>
                        </span>
                    </div>
                    <div class="col-6 col-md-4 copyright text-center">
                        <a target="_blank" href="https://findynamix.com/"><p>FinDynamix © 2020</p></a>
                    </div>
                    <div class="right col-6 col-md-4 pb-2 pb-md-2">
                        <form onsubmit="return false" class="d-none d-md-block">
                            <span data-toggle="modal" data-target="#exampleModalCenter">
                                <input type="text" placeholder="e-mail">
                                <button>ABUNƏ OLUN</button>
                            </span>
                        </form>
                    </div>
                </div>
            </div>
            <img class="d-none d-md-block" style="width: 100%; position: absolute; bottom: 0; z-index: -1;" src="assets/img/bg/footer-bg.svg" alt="">
            <img class="d-block d-md-none" style="width: 100%" src="assets/img/bg/m-footer.png" alt="">
        </footer>
    </div>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="assets/css/bootstrap-4.3.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    <script src="assets/js/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="assets/js/main.js"></script>


    <script>
        $(document).ready(function(){

            $('.slider').bxSlider({
                slideSelector: '.slider-item',
                responsive: true,
                mode: 'fade',
                auto: true,
                pager: false,
                touchEnabled: false,
            });

            $('.slider-container .slide-item-1 .phones').addClass(['animate__animated', 'animate__fadeInLeftBig']);
            $('.slider-container .slide-item-1 .title-2').addClass('');
            $('.slider-container .slide-item-1 .title-1').addClass(['animate__animated', 'animate__heartBeat']);
            $('.slider-container .slide-item-1 .title-3').addClass('');
            $('.slider-container .slide-item-1 .download-btn').addClass('');

            /*$('.slider-container .slide-item-2 .slider-2-img-1').addClass(['animate__animated', 'animate__fadeInUpBig'], 500);
            $('.slider-container .slide-item-2 h1').addClass('animate__heartBeat', 800);

            $('.slider-container .slide-item-3 .slider-3-img-1').addClass(['animate__animated', 'animate__fadeInUpBig'], 500);

            $('.slider-container .slide-item-3 .overlay-2 img:first-child').addClass(['animate__animated', 'animate__fadeInLeftBig'], 800);
            $('.slider-container .slide-item-3 .overlay-2 img:nth-child(2)').addClass(['animate__animated', 'animate__fadeInLeftBig'], 500);
            $('.slider-container .slide-item-3 .overlay-2 img:nth-child(3)').addClass(['animate__animated', 'animate__fadeInLeftBig'], 200);


            $('.slider-container .slide-item-4 .slider-4-img-1').addClass(['animate__animated', 'animate__fadeInUpBig'], 500);

            $('.slider-container .slide-item-4 .overlay-2 img:first-child').addClass(['animate__animated', 'animate__fadeInBig'], 200);
            $('.slider-container .slide-item-4 .overlay-2 h1').addClass(['animate__animated', 'animate__fadeInBig'], 500);


            $('.slider-container .slide-item-5 .overlay-2 img:first-child').addClass(['animate__animated', 'animate__fadeInLeftBig'], 800);
            $('.slider-container .slide-item-5 .overlay-2 img:nth-child(2)').addClass(['animate__animated', 'animate__fadeInLeftBig'], 600);
            $('.slider-container .slide-item-5 .overlay-2 img:nth-child(3)').addClass(['animate__animated', 'animate__fadeInLeftBig'], 400);
            $('.slider-container .slide-item-5 .overlay-2 img:nth-child(4)').addClass(['animate__animated', 'animate__fadeInLeftBig'], 200);*/

            $('.preloader').hide();
        });
    </script>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <button type="button" class="close p-2" data-dismiss="modal" aria-label="Close">
                        <span style="font-size: 32px" aria-hidden="true">&times;</span>
                    </button>
                    <iframe style="border: none; width: 100%; height: 532px" src="https://e6bd9c8a.sibforms.com/serve/MUIEAP88MiKqJfcrGEO6ZsYtDK5VeIZx1tC-yshOFCMZXnic89ayDt6CvcN_keIzh8Y1_MOI3F_4bFOxirF166hZLwpOmnC8hZuGuajwmDHpxUfGF6PaBhJxWU4-hjgFUyeOsf5YqElaLx4K7H5Y-zcsDDEPOoQOExPfB7q-bUcbZ7F0jr_obDF_nqvIeH6O29UoSWs2e5XZWGbo"/>
                </div>
            </div>
        </div>
    </div>
</body>
</html>